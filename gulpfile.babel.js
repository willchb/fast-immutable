import del from "del";
import gulp from "gulp";
import babel from "gulp-babel";
import eslint from "gulp-eslint";
import sourcemaps from "gulp-sourcemaps";
import gutil from "gulp-util";

const SOURCE_PATH = "source";
const OUTPUT_PATH = "dist";

const BABEL_CONFIG = {
    babelrc: false,
    plugins: [ "transform-es2015-modules-umd" ],
    presets: [ "es2015" ]
};

gulp.task("clean", () => del.sync(OUTPUT_PATH));

const babelTask = (source, dest) => gulp.src(source)
    .pipe(sourcemaps.init())
    .pipe(babel(BABEL_CONFIG)).on("error", function(error) {
        gutil.log(
            gutil.colors.red.bold(`${error.name}: `),
            gutil.colors.reset.magenta(`${error.message}`),
            gutil.colors.reset(`\n${error.codeFrame}`)
        );
        this.emit("end");
    })
    .pipe(sourcemaps.write(SOURCE_PATH))
    .pipe(gulp.dest(dest));

gulp.task("babel", () => babelTask([ `${SOURCE_PATH}/**/*.js` ], OUTPUT_PATH));

gulp.task("eslint", () => gulp.src([ `${SOURCE_PATH}/**/*.js` ])
    .pipe(eslint())
    .pipe(eslint.format())
);

gulp.task("watch", function() {
    const watchers = [
        gulp.watch(`${SOURCE_PATH}/**/*.js`, [ "babel" ])
    ];
    for (const watcher of watchers) {
        watcher.on("change", outputChange);
    }
    function outputChange(event) {
        console.log(`File ${event.path} was ${event.type}`);
    }
});

gulp.task("default", [ "clean", "eslint", "babel" ]);
