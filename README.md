Fast Immutable JavaScript Objects
====================================

Fast Immutable provides you with a way of creating immutable objects in Javascript.
Immutable is useful when one wants to protect the internal state of the application against arbitrary change.

Fast Immutable works with plain Object, as well as Array and Date.

Read more about Immutable Object here: http://en.wikipedia.org/wiki/Immutable_object

Getting started
---------------

Install `fast-immutable` using npm.

```shell
npm install fast-immutable
```

Then use it in your module.

```js
import { Immutable } from "fast-immutable";

const immutableA = new Immutable({
    propA: "Value of A",
    propB: "Value of B",
    propC: {
        propA: "Value of C.A"
    }
});

const immutableB = immutableA.set("propC.propA", "Value of C.A Changed");

console.debug(immutableA.get());
console.debug(immutableB.get());
```
