
function throwCannotChange() {
    throw new TypeError("Cannot change immutable");
}

const objectHandler = {
    set: throwCannotChange,
    deleteProperty: throwCannotChange
};

const dateHandler = Object.assign({ }, objectHandler, {
    get(target, property) {
        if (typeof target[property] === "function") {
            if (property.substr(0, 3) === "set") {
                return throwCannotChange;
            }
            return target[property].bind(target);
        }
        return target[property];
    }
});

const arrayHandler = Object.assign({ }, objectHandler, {
    get(target, property) {
        if (typeof target[property] === "function") {
            switch (property) {
            case "splice": case "shift": case "unshift":
                return throwCannotChange;
            }
        }
        return target[property];
    }
});

const ImmutableMixin = Template => class extends Template {
    get __immutable__() {
        return true;
    }
    get(path) {
        let object;
        if (path) {
            const props = path.split(".");
            const prop = props.shift();
            switch (typeof this[prop]) {
            case "object":
                if (this[prop].__immutable__) {
                    object = this[prop].get(props.join("."));
                } else {
                    object = this[prop];
                }
                break;
            case "string": case "number": case "boolean":
                object = this[prop];
                break;
            }
        } else {
            if (this instanceof Date) {
                object = new Date(this);
            } else if (this instanceof Array) {
                object = [ ];
            } else {
                object = { };
            }
            for (const prop of Object.keys(this)) {
                if (this[prop].__immutable__) {
                    object[prop] = this[prop].get();
                } else {
                    object[prop] = this[prop];
                }
            }
        }
        return object;
    }
    set(path, value) {
        const props = path.split(".");
        const prop = props.shift();
        const object = Object.assign({ }, this);
        object[prop] = props.length ? this[prop].set(props.join("."), value) : value;
        return new Immutable(object);
    }
};

class ImmutableObject {
    constructor(object) {
        if (object === null || typeof object !== "object") {
            throw new TypeError("An object must be passed to Immutable constructor");
        }
        if (object.__immutable__) {
            return object;
        }
        let immutable = this;
        let handler = objectHandler;
        if (object instanceof Date) {
            immutable = new ImmutableDate(object);
            handler = dateHandler;
        } else if (object instanceof Array) {
            immutable = new ImmutableArray(object);
            handler = arrayHandler;
        }
        for (const prop of Object.keys(object)) {
            switch (typeof object[prop]) {
            case "object":
                immutable[prop] = object[prop] ? new Immutable(object[prop]) : object[prop];
                break;
            case "string": case "number": case "boolean":
                immutable[prop] = object[prop];
                break;
            }
        }
        return new Proxy(Object.freeze(immutable), handler);
    }
}

class ImmutableArray extends ImmutableMixin(Array) { }
class ImmutableDate extends ImmutableMixin(Date) { }
class Immutable extends ImmutableMixin(ImmutableObject) { }

export { ImmutableArray, ImmutableDate, Immutable };
