import assert from "assert";
import { Immutable } from "../immutable";

const WIDTH = 10;
const DEPTH = 3;

function objGen(width, depth, object = { }) {
    if (depth) {
        for (let i = 0; i < width; i++) {
            objGen(width, depth - 1, object[`prop${String.fromCharCode(i + 65)}`] = { });
        }
    } else {
        for (let i = 0; i < width; i++) {
            object[`prop${String.fromCharCode(i + 65)}`] = `Value of Property ${String.fromCharCode(i + 65)}`;
        }
    }
    return object;
}

const pojoA = objGen(WIDTH, DEPTH);
const pojoB = objGen(WIDTH, DEPTH);

pojoB.propD.propD = new Date();

const immutableA = new Immutable(pojoA);
const immutableB = new Immutable(pojoB);

assert.deepEqual(pojoA, immutableA);
assert.deepEqual(pojoB, immutableB);

assert.throws(
    () => { immutableA.propK = { }; },
    (e) => e instanceof TypeError && e.message === "Cannot change immutable",
    "Expected TypeError with message 'Cannot change immutable' to be thrown"
);

assert.throws(
    () => { immutableB.propC.propD.propK = { }; },
    (e) => e instanceof TypeError && e.message === "Cannot change immutable",
    "Expected TypeError with message 'Cannot change immutable' to be thrown"
);

assert.throws(
    () => { immutableA.propC.propD.propE = true; },
    (e) => e instanceof TypeError && e.message === "Cannot change immutable",
    "Expected TypeError with message 'Cannot change immutable' to be thrown"
);

assert.throws(
    () => { immutableB.propD.propD.newProp = true; },
    (e) => e instanceof TypeError && e.message === "Cannot change immutable",
    "Expected TypeError with message 'Cannot change immutable' to be thrown"
);

assert.throws(
    () => { immutableB.propD.propD.setMonth(1); },
    (e) => e instanceof TypeError && e.message === "Cannot change immutable",
    "Expected TypeError with message 'Cannot change immutable' to be thrown"
);
